//
//  UIColor+Hex.swift
//  CTool
//
//  Created by IT on 31/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import UIKit




// MARK: - UIColor 扩展
extension UIColor {
    
    public static let FF: UInt = 0xFF;
    
    /// 将UInt 转换成颜色值
    ///
    /// - Parameters:
    ///   - hex: hex UInt
    ///   - alpha: alpha
    public convenience init(uint hexValue: UInt, alpha alphaValue: CGFloat = 1.0) {
        let red: CGFloat = CGFloat((hexValue >> 16) & UIColor.FF) / CGFloat(UIColor.FF)
        let green: CGFloat = CGFloat((hexValue >> 8) & UIColor.FF) / CGFloat(UIColor.FF)
        let blue: CGFloat = CGFloat(hexValue & UIColor.FF) / CGFloat(UIColor.FF)
        if #available(iOS 10.0, *) {
            self.init(displayP3Red: red, green: green, blue: blue, alpha: alphaValue)
        } else {
            self.init(red: red, green: green, blue: blue, alpha: alphaValue)
        }
    }
    
    /// 如果字符串可以转换成UInt 按照UInt 来进行转换 否则为 1
    ///
    /// - Parameters:
    ///   - hex: hexString
    ///   - alpha: alpha
    public convenience init(string hexString: String, alpha alphaValue: CGFloat = 1.0) {
        if let hexUInt = UInt(hexString, radix: 16) {
            self.init(uint: hexUInt, alpha: alphaValue)
        } else {
            self.init(white: 1, alpha: alphaValue)
        }
    }
    
    public struct ColorValue {
        
    }
}

/// 重命名 UIColor.ColorValue 存放颜色值
public typealias ColorValue = UIColor.ColorValue

/// 重命名 UInt
public typealias Color = UInt

