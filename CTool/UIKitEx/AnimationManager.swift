//
//  AnimationManager.swift
//  CTool
//
//  Created by IT on 04/01/2019.
//  Copyright © 2019 Person. All rights reserved.
//

import UIKit

public class AnimationManager: NSObject {
    
    /// 单例类
    public static let sharedManager = AnimationManager()
    
    public static let duration: TimeInterval = 0.3

//    class PresentAnimation: NSObject, UIViewControllerAnimatedTransitioning {
//        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//           return AnimationManager.duration
//        }
//
//        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//            <#code#>
//        }
//    }
//
//    class DismissAnimation: NSObject, UIViewControllerAnimatedTransitioning {
//        func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//           return AnimationManager.duration
//        }
//
//        func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//            <#code#>
//        }
//    }
    
    public enum ModalStyle {
        case present
        case dismiss
    }
    
    
    public class ModalAnimation: NSObject, UIViewControllerAnimatedTransitioning {
        
        
        private let style: ModalStyle
        
        public init(with style:ModalStyle) {
            self.style = style
        }
        
        public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
            return AnimationManager.duration
        }
        
        public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//            UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//            UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//            UIView *container = [transitionContext containerView];
//
//            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
//            CGRect fromVCRect = fromVC.view.frame;
//            fromVCRect.origin.x = 0;
//            fromVC.view.frame = fromVCRect;
//            [container addSubview:toVC.view];
//
//            CGRect toVCRect = toVC.view.frame;
//            toVCRect.origin.x = screenWidth;
//            toVC.view.frame = toVCRect;
//
//            fromVCRect.origin.x = -screenWidth;
//            toVCRect.origin.x = 0;
//
//            [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
//                fromVC.view.frame = fromVCRect;
//                toVC.view.frame = toVCRect;
//                } completion:^(BOOL finished){
//                [fromVC.view removeFromSuperview];
//                [transitionContext completeTransition:finished];//动画结束、取消必须调用
//                }];
            
            
            guard let from = transitionContext.view(forKey: .from), let to = transitionContext.view(forKey: .to) else {
                return
            }
            
            let container = transitionContext.containerView
            let xTranslation = UIScreen.main.bounds.width
            
            container.addSubview(from)
            container.addSubview(to)
            
            switch self.style {
            case .dismiss:
                to.transform = CGAffineTransform.init(translationX: xTranslation, y: 0)
                UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                    from.transform = CGAffineTransform(scaleX: -xTranslation, y: 0)
                    to.transform = CGAffineTransform.identity
                }) {
                    from.transform = CGAffineTransform.identity
                    transitionContext.completeTransition($0)
                }
                break
            case .present:
                to.transform = CGAffineTransform.init(translationX: xTranslation, y: 0)
                UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                    from.transform = CGAffineTransform(scaleX: -xTranslation, y: 0)
                    to.transform = CGAffineTransform.identity
                }) {
                    from.transform = CGAffineTransform.identity
                    transitionContext.completeTransition($0)
                }
                break
            }
            
            
        }
    }
}


extension AnimationManager: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .present)
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimationManager.ModalAnimation(with: .dismiss)
    }
}

