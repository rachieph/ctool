//
//  String+URL.swift
//  CTool
//
//  Created by IT on 31/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import Foundation

extension String {
    
    /// 图片链接编码 对图片中有特殊字符进行编码
    ///
    /// - Returns: 图片链接转码
    public func imageURLEncoding() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    /// 对已有格式的字符串格式化
    ///
    /// - Parameters:
    ///   - characterSet: 包含的字符
    ///   - offset: 偏移
    /// - Returns: 格式化后的字符串
    public func format(trimming character: String, offsetby offset:Int) -> String {
        var variable = self
        var contents = [String]()
        if variable.count > 0 {
            let characterSet = CharacterSet(charactersIn: character)
            if let _ = variable.rangeOfCharacter(from: characterSet) {
                variable = variable.trimmingCharacters(in: characterSet)
            }
            while variable.count > 0 {
                if let start = variable.index(variable.startIndex, offsetBy: offset, limitedBy: variable.endIndex) {
                    contents.append(String(variable[..<start]))
                    variable = String(variable[start...])
                } else {
                    contents.append(variable)
                    variable = ""
                }
            }
            return contents.joined(separator: character)
        } else {
            return self
        }
    }

    /// 移除空格
    ///
    /// - Returns: 将空格移除
    public func removeWhiteSpace() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
}
