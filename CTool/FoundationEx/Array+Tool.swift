//
//  Array+JSON.swift
//  CTool
//
//  Created by IT on 31/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import Foundation


extension Array {
    
    /// 将数组转换成 json字符串
    ///
    /// - Returns: 如果成功转换成字符串 否则返回 nil
    /// - Throws: error
    public func jsonString() throws -> String? {
        let data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        return String(data: data, encoding: .utf8)
    }
    
}
