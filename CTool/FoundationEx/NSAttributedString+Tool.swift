//
//  AttributedString.swift
//  CTool
//
//  Created by IT on 04/01/2019.
//  Copyright © 2019 Person. All rights reserved.
//

import UIKit

// MARK: - NSAttributedString 扩展
extension NSAttributedString {
    
    /// 将html 转变成NSAttributedString
    ///
    /// - Parameter string: html
    /// - Throws: error
    public convenience init(with html: String) throws {
        try self.init(data: html.data(using: .utf8)!,
                      options: NSAttributedString.htmlDocumentReadingOptionKey,
                      documentAttributes: nil)
    }
    
    /// 将NSAttributedString转变成html String (并不能完全还原)
    ///
    /// - Returns: 成功返回string 不成功返回nil
    /// - Throws: error
    public func htmlString() throws -> String? {
        let data = try self.data(from: NSRange(location: 0, length: self.length), documentAttributes: NSAttributedString.htmlDocumentAttributeKey)
        return String(data: data, encoding: .utf8)
    }
    
    /// html 转 NSAttributedString 字典
    public static let htmlDocumentReadingOptionKey: [NSAttributedString.DocumentReadingOptionKey: Any] = [.documentType : DocumentType.html, .characterEncoding : String.Encoding.utf8.rawValue]
    
    /// NSAttributedString 转 html 字典
    public static let htmlDocumentAttributeKey: [NSAttributedString.DocumentAttributeKey : Any] = [.documentType : DocumentType.html, .characterEncoding : String.Encoding.utf8.rawValue]
}
