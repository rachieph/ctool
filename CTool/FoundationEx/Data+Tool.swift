//
//  Data+Key.swift
//  CTool
//
//  Created by IT on 31/12/2018.
//  Copyright © 2018 Person. All rights reserved.
//

import Foundation

extension Data {
    
    /// 取 Data 中对应的key值 转换成data
    ///
    /// - Parameter key: 要取的key值
    /// - Returns: 返回的data
    public func data(forKey key: String) throws -> Data? {
        var result: Data? = nil
        let object = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableLeaves)
        if let dic = object as? Dictionary<String, Any>, let val = dic[key] {
            result = try JSONSerialization.data(withJSONObject: val, options: .prettyPrinted)
        }
        return result
    }
}
