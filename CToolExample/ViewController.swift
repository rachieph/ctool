//
//  ViewController.swift
//  CToolExample
//
//  Created by IT on 04/01/2019.
//  Copyright © 2019 Person. All rights reserved.
//

import UIKit
import CTool

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        
        if let _ = self.presentedViewController {
            let color = UIColor(uint: 0xFFAD00, alpha: 0.5)//(white: 0xFFAD00, alpha: 0.5)
            self.view.backgroundColor = color
        } else {
            let color = UIColor(uint: 0xFFFF00, alpha: 0.5)//(white: 0xFFAD00, alpha: 0.5)
            self.view.backgroundColor = color
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    @objc func tap() {
        if let _ = self.presentingViewController {
            self.dismiss(animated: true, completion: nil)
        }
        let vc = ViewController()
        vc.transitioningDelegate = AnimationManager.sharedManager
        
        
        self.present(vc, animated: true, completion: nil)
        
    }

}

