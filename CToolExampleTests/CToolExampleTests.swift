//
//  CToolExampleTests.swift
//  CToolExampleTests
//
//  Created by IT on 04/01/2019.
//  Copyright © 2019 Person. All rights reserved.
//

import XCTest
@testable import CToolExample

class CToolExampleTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        
        let keyvalues: [String : Any] = ["key1": "value1", "key2": "value2", "data": [1, 2, 3, 4, 5]]
        var data: Data? = nil
        
        var result: String? = nil
        
        do {
            data = try JSONSerialization.data(withJSONObject: keyvalues, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            
            if let d = data {
                data = try AppDelegate.testFunction(data: d, key: "data")
            }
            if let d = data {
                result = String(data: d, encoding: .utf8)
            }
            
        } catch  {
            print("ddd is \(error)")
        }
        
        
        
        XCTAssertNotNil(result)
        
        self.measure {
            // Put the code you want to measure the time of here.
            
        }
    }
    
}
